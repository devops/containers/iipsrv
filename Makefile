SHELL = /bin/bash

build_tag ?= iipsrv

chart_name = $(shell cat ./chart/Chart.yaml | grep ^name | awk '{print $$2}')
chart_version = $(shell cat ./chart/Chart.yaml | grep ^version | awk '{print $$2}')

.PHONY: build
build:
	DOCKER_BUILDKIT=1 docker build --pull -t $(build_tag) .

.PHONY: test
test:
	./test/run
