FROM debian:bookworm AS base

ENV LANG=en_US.UTF-8 \
    LANGUAGE=en_US:en \
    TZ=US/Eastern

RUN apt -y update && \
    apt -y install \
    libgomp1 \
    libmemcached-dev \
    libopenjp2-7-dev \
    libpng-dev \
    libtiff-dev \
    libturbojpeg-dev \
    libwebp-dev

FROM base AS build

RUN apt -y install \
    autoconf \
    autotools-dev \
    curl \
    g++ \
    libtool \
    make \
    pkg-config

WORKDIR /usr/src/iipsrv

RUN set -eux; \
    curl -sfL -o iipsrv.tar.gz https://github.com/ruven/iipsrv/archive/refs/tags/iipsrv-1.2.tar.gz; \
    echo "68dcb988e20734585718b8b17c1100ac07d4b8d5fe5732d9570b8f68a98675d4  iipsrv.tar.gz" \
      | sha256sum -c --strict ; \
    tar -zxf iipsrv.tar.gz --strip-components=1 ; \
    rm iipsrv.tar.gz; \
    ./autogen.sh; \
    ./configure; \
    make

FROM base AS iipsrv

COPY --from=build /usr/src/iipsrv/src/iipsrv.fcgi /usr/local/bin/iipsrv.fcgi

LABEL org.opencontainers.artifact.title="IIPImage Server"
LABEL org.opencontainers.artifact.description="IIPImage Server built from source"
LABEL org.opencontainers.image.source="https://github.com/ruven/iipsrv"
LABEL org.opencontainers.image.version="1.2"
LABEL org.opencontainers.image.license="GPLv3"

#
# IIPImage Server variables
# https://github.com/ruven/iipsrv#configuration
#
ENV LOGFILE="/dev/stdout" \
    IIIF_VERSION="2" \
    URI_MAP="iiif=>IIIF" \
    VERBOSITY="1"

RUN rm -rf /var/lib/apt/lists/*; \
    useradd -r -g 0 iipsrv; \
    mkdir -p -m 0444 /data

USER iipsrv

VOLUME /data

EXPOSE 9000

CMD [ "/usr/local/bin/iipsrv.fcgi", "--bind", "0.0.0.0:9000" ]
